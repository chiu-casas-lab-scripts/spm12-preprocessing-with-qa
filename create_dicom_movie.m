function create_dicom_movie(dicom_list, out_file, overwrite, protocol)
% CREATE_DICOM_MEAN_MOVIE
%     DICOM_LIST <nx1 cell array or nxm char array>: List of dicom files to
%     include in the movie, in the order of desired presentation.
%
%     OUTFILE <1xn char array>: Path to save resulting AVI movie to.
%
%     OVERWRITE <boolean>: If truthy, wil ovewrite existing image. Defaults
%     to false.
% 
% Creates an avi movie from list of DICOM images. I have only tested with
% functional and structural MRI.
%
max_cutoff = 500;
dicom_list = cellstr(dicom_list);
if exist(out_file, 'file') && ~overwrite
    error('Output file %s already exists', out_file);
end
if strcmp(protocol, 'functional')
    n = numrows(dicom_list);
    try
        video = VideoWriter(out_file, 'Grayscale AVI');
    catch
        video = VideoWriter(out_file, 'Uncompressed AVI');
    end
    open(video);
    try
        t = dicomread(dicom_list{1});
        if ndims(t) == 4
            X_raw = zeros(size(t,1), size(t,2), size(t,3), size(t,4), n);
            X_raw(:, :, :, :, 1) = t;
            for ii = 2 : n
                this_data = dicomread(dicom_list{ii});
                if ~(all(size(this_data) == size(t)))
                    X_raw(:, :, :, :, ii) = [];
                else
                    X_raw(:, :, :, :, ii) = this_data;
                end
            end
            
            min_X = 0;
            max_X = max(X_raw(:));
            mid_slice_num = floor(size(X_raw, 4) / 2);
            for ii = 1 : n
                %Xii_interp = interpolate(min_X, max_X, 0, 255, min(cutoff, X_raw(:, :, 1, mid_slice_num, ii)));
                %Xii_uint8 = uint8(Xii_interp);
                %writeVideo(video, Xii_uint8);
                writeVideo(video, double(X_raw(:, :, 1, mid_slice_num, ii)) / double(max_X));
            end
            close(video);
        else
            assert(ndims(t) == 3);
            X_raw = zeros(numrows(t), numcols(t), n);
            X_raw(:, :, 1) = t;
            for ii = 2 : n
                X_raw(:, :, ii) = dicomread(dicom_list{ii});
            end
            min_X = 0;
            max_X = max(X_raw(:));
            for ii = 1 : n
                %Xii_interp = interpolate(min_X, max_X, 0, 255, min(cutoff, X_raw(:, :, ii)));
                %Xii_uint8 = uint8(Xii_interp);
                %writeVideo(video, Xii_uint8);
                writeVideo(video, double(X_raw(:, :, ii)) / double(max_X))
            end
            close(video)
        end
    catch
        close(video)
    end
end

if strcmp(protocol, 'structural')
    n = numrows(dicom_list);
    try
        video = VideoWriter(out_file, 'Grayscale AVI');
    catch
        video = VideoWriter(out_file, 'Uncompressed AVI');
    end
    open(video);
    try
        t = dicomread(dicom_list{1});
        if ndims(t) == 4
            assert(length(dicom_list) == 1);
            squeezed = squeeze(t);
            min_X = 0;
            max_X = max(squeezed(:));
            % Unfortunaely, uint16 with large values won't scale well
            % to uint8; and resulting images lack contrast.
            cutoff = min(max_cutoff, floor(quantile(squeezed(:), 0.95)));
            for ii = 1 : size(squeezed, 3)
                %Xii_interp = interpolate(min_X, max_X, 0, 255, min(cutoff, squeezed(:, :, ii)));
                %Xii_uint8 = uint8(Xii_interp);
                %writeVideo(video, Xii_uint8);
                writeVideo(video, double(squeezed(:, :, ii)) / double(max_X));
            end
            close(video);
        else
            assert(ndims(t) == 3);
            X_raw = zeros(numrows(t), numcols(t), n);
            X_raw(:, :, 1) = t;
            for ii = 2 : n
                X_raw(:, :, ii) = dicomread(dicom_list{ii});
            end
            min_X = 0;
            max_X = max(X_raw(:));
            % Unfortunaely, uint16 with large values won't scale well
            % to uint8; and resulting images lack contrast.
            cutoff = min(max_cutoff, floor(quantile(X_raw(:), 0.95)));
            for ii = 1 : n
                %Xii_interp = interpolate(min_X, max_X, 0, 255, min(cutoff, X_raw(:, :, ii)));
                %Xii_uint8 = uint8(Xii_interp);
                %writeVideo(video, Xii_uint8);
                writeVideo(video, double(X_raw(:, :, ii)) / double(max_X));
            end
            close(video)
        end
    catch
        close(video)
    end
end

end

function x = interpolate(orig_min, orig_max, new_min, new_max, data)
x = ((data - orig_min) .* (new_max - new_min)) ./ (orig_max - orig_min) + new_min;
end
