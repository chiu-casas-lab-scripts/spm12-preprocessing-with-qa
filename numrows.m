function n = numrows(x)
% N = NUMROWS  Returns the number of rows of a 2d data object
%     X <2d data object>: for example, a matrix, cell array, dataset or
%     table.
%
n = size(x, 1);
end