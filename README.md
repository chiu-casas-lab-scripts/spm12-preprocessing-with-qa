# Quality Assurance Scripts and Resources for SPM12 fMRI Preprocessing

This repository includes a number of scripts and resources that can be incorporated into a qualitative quality assurance protocol for the preprocessing of functional and structural MRI.

![Example ROI overlay over normalized subject structural](resources/readme/norm-struct-overlay-small.png "ROI Overlay Over Normalized Structural")


![Example functional overlay over template structural](resources/readme/norm-func-overlay-small.png "Normalized Functional Overlay")

 Resources include:

* MATLAB and FSL scripts to create overlay of functional and structural subject images to assess co-registration
* MATLAB and FSL scripts to create overlays of select ROIs over subject's normalized structural to assess accuracy of normalization
* MATLAB and FSL scripts to create overlays of normalized functionals over the normalization template image to assess success of normalization
* MATLAB scripts to create mean DICOm functional images and AVI movies of structural and functional DICOM data to rapidly assess for the presence of artifacts (ringing, blowout, RF interference, motion artifact, etc.)
* An Excel spreadsheet that implements a modified version of the [Backhausen et al QC Rating System](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5138230/), described briefly below.
* Two auto-reorient scripts, one for the structural, and another for functional images (these have to be treated differently in order for realignment parameters to remain meaningful). 
* An example SPM12 preprocessing script incorporationg the above scripts into its output.

## Modified Backhausen QC Rating System

Structural DICOM and functional DICOM images may be assessed using the following scheme. Examples of the sort of artifacts referred to below can be found on these external resources:

+ [CBS MRI Quality Control Workshop \(pdf\)](http://cbs.fas.harvard.edu/usr/mcmains/CBS_MRI_Quality_Control_Workshop.pdf)
+ [CBS MRI Qualitative Control Manual \(pdf\)](http://cbs.fas.harvard.edu/usr/mcmains/CBS_MRI_Qualitative_Quality_Control_Manual.pdf)
+ [Gibbs effect/Ringing](https://en.wikipedia.org/wiki/Gibbs_phenomenon)

### Structural MRI

Structural images are scored according to the following scale:

+ C1 (pass) - proceed to preprocessing
+ C2 (check) - replace with other structurals if appropriate, available, and useful, consider retrospective correction, mark for additional QC
+ C3 (fail) - replace with other structural if appropriate, available, and useful.

based upon the following components:

+ Image Sharpness
  + R1 (pass) - Clear/ rather clear image; ghosts, blurred regions or other artifacts if at all minor; no susceptibility artifacts
  + R2 (check) - Rather coarse/ blured image; moderate motion artifacts; if susceptibility artifacts are present they do not influence relevant areas
  + R3 (fail) - Obviously coarse/blurred image; major motion and susceptibility artifacts (e.g. due to dental braces)

+ Ringing
  + R1 (pass) - No/ slight ringing artifacts seen; at most in one region
  + R2 (check) - Ringing artifacts in more than one region
  + R3 (fail) -  Circular ringing artifacts throught the whole image

+ Subcortical Contrast to Noise Ratio
  + R1 (pass) - Sharp edges; structures can be well identified
  + R2 (check) - Structures still can be identified but less clear
  + R3 (fail) - Structures can hardly be identified

+ GM/WM Contrast to Noise Ratio
  + R1 (pass) - Sharp edges; gray matter and white matter are well differentiated
  + R2 (check) - Gray matter and White matter not well differentiated
  + R3 (fail) - Borders of gray matter and white matter blend; not differentiated at all

+ Head Coverage
  + R1 (pass) - All of brain (or intended region of interest) in field of view.
  + R2 (check) - Most of brain (or intended region of interest) is in field of view. Parts outside field of view are less important to study aims.
  + R3 (fail) - Essential anatomical regions of the brain are not in the field of view.

Each of these components can be weighted to suit study aims. The weighted average of these components should be mapped to the three summary categories. 

Use the following helpful documents to guide ratings:

+ Examples of each of these ratings [can be found here \(pdf\)](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5138230/bin/Presentation1.PDF).
+ Definitions [can be found here \(pdf\)](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5138230/bin/Table1.pdf).

### Functional Images

Similarly, functional images should be categorized into the following bins:

+ C1 (pass) - proceed to preprocessing
+ C2 (check) - mark for more additional QC
+ C3 (fail) - exclude

based on the following criteria:

+ Image Sharpness
  - Susceptibility Artifacts
  - Signal Intensity Inhomogeneity
  - Ghosting
  - Dropout and Deformation at Air-Tissue Interfaces
+ Intermittent Artifacts
  - RF Frequency Noise / Spiking
  - Motion Slice Artifact
+ Head Coverage (brain in field of view)

using the following rating system:

+ Image Sharpness
  + R1 (pass) - Clear/ rather clear image; ghosts, dropout or deformation at air-tissue interfaces not severe, blurred regions or other artifacts if at all minor; no susceptibility artifacts induced by ferromagnetic objects. Signal intensity inhomogeneity not atypical.
  + R2 (check) - Rather coarse/ blurred image; moderate dropout and deformation at air-tissue interface; susceptibility artifacts if present do not affect regions of interest; Signal intensity inhomogeneity moderate but not severe
  + R3 (fail) - Obviously coarse/blurred or distorted image; severe dropout or distortion removes regions of interest; disqualifying susceptibility artifacts; unexpected and severe intensity inhomogeneity.

+ Intermitten Artifacts
  + R1 (pass) - None or little RF noise/spiking; no observed motion slice artifacts.
  + R2 (check) - Some RF noise or spiking or motion slice artifacts are present, but are likely resolvable with repair or censoring.
  + R3 (fail) - Spiking or motion slice artifacts are ubiquitous, rending data useless.

+ Head Coverage (brain in field of view)
  + R1 (pass) - All of brain (or intended region of interest) in field of view.
  + R2 (check) - Most of brain (or intended region of interest) is in field of view. Parts outside field of view are less important to study aims.
  + R3 (fail) - Essential anatomical regions of the brain are not in the field of view.

Each of these components can be weighted to suit study aims.

### Co-registration
Successful normalization depends on the functional and structural images be successfully co-registered. Therefore it is import to assess. Successful co-registration often depends on the structural and functional images starting in rough correspondence. The auto-reorient script is intended to increase the chance of successful co-registration. The `create_coreg_overlay.sh`/`create_coreg_overlay.m` scripts faciltate the evaluation of the success of coregistration, and can be rated on the following 3-point scale:

+ R1 (pass) - co-registration show no significant defects
+ R2 (check) - co-registration is fair, but may be difficult to assess because of functional artifacts. Consider redoing.
+ R3 (fail) - co-registration obviously failed. Determine the cause, and address.

Options to address bad orientation include manually re-orienting the images and re-running the preprocessing routing (with auto-reorient disabled), or manually co-registering and running again (either with the co-registration routine included, or not).


### Normalization
The `create_normcheck_overlays.sh`/`create_normcheck_overlays.m` scripts create various overlays that facilitate assessment of the quality of normalization. Two sorts of overlays are created:

1. A set of ROIs are overlain on top of the subjects normalized structural (SPM can be directed to output this by adding and additional step in the normalization routine). These ROIs are anatomical markers which should be satisfied if normalization is successful (see example image above).

2. Overlays of the normalized function over the template structural.

Each overlay image can be assessed on a 3 point scale:

+ R1 (pass) - normalization successful
+ R2 (check) - normalization problematic. double check preprocessing steps
+ R3 (fail) - Unusuable unless corrected.

See the excel sheet for an example of how this system can be implemented.
