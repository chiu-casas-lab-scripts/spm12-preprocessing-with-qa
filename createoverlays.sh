# Primitive script to create overlays provided that your preprocessing 
# script was ran with the 'outsh' option, and
# created .sh scripts in each subjects preprocessing folder.
# Assumes that all folders in the fullpath you pass to the script are 
# subject folders.
#
# Call like: bash createoverlays.sh /mnt/nfs/yourproj/preprocessingdir
#

PREPDIR=$1
for taskdir in "$PREPDIR"/*; do
    bash "$taskdir"/coreg_overlay.png.sh
    bash "$taskdir"/normoverlay.sh
    sleep 0.5
done
