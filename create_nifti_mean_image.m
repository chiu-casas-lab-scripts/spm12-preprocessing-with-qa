function create_nifti_mean_image(images, out_file, overwrite)
% CREATE_NIFTI_MEAN_IMAGE  Creates a mean nifti image from a list of
% images.
%
%     IMAGES <nx1 cell array or nxm char array>: List of dicom files to
%     inclue in the mean image.
%
%     OUTFILE <1xn char array>: Path to save resulting nifti image to.
%
%     OVERWRITE <boolean>: If truthy, wil ovewrite existing image. Defaults
%     to false.
%
if nargin < 3
    overwrite = false;
end
if isempty(images)
    error('You need at least one nifti image file');
end
if iscell(images)
    try
        images = char(images);
    catch
        error('nifti_image_list should be char array as returned by spm_select');
    end
end
if exist(out_file, 'file') && ~overwrite
    error('Output file %s already exists', out_file);
end
V = spm_vol(images);
X = spm_read_vols(V);
V_target = V(1);
V_target.fname = out_file;
X_mean = round(mean(X, 4));
spm_write_vol(V_target, X_mean);
end
