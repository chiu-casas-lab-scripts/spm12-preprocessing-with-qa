#! /usr/bin/env bash
FSLDIR=/usr/local/fsl
. ${FSLDIR}/etc/fslconf/fsl.sh
PATH=$PATH:$FSLDIR/bin;
export FSLDIR PATH;
export QT_X11_NO_MITSHM=1
