function create_dicom_mean_image(dicom_list, out_file, overwrite)
% CREATE_DICOM_MEAN_IMAGE  Creates a mean image from a list of DICOM 
% images.
%
%     DICOM_LIST <nx1 cell array or nxm char array>: List of dicom files to
%     inclue in the mean image.
%
%     OUTFILE <1xn char array>: Path to save resulting image to. Must have
%     extension in {jpg, jpeg, gif, png, bmp}. jpg or png preferred.
%
%     OVERWRITE <boolean>: If truthy, will ovewrite existing image. Defaults
%     to false.
% 
% I have only tested with functional and structural images.
%

if nargin < 3
    overwrite = false;
end
dicom_list = cellstr(dicom_list);
if exist(out_file, 'file') && ~overwrite
    error('Output file %s already exists', out_file);
end
[~, ~, ext] = fileparts(out_file);
if sum(strcmpi(ext, {'.jpg', '.jpeg', '.gif', '.png', '.bmp'})) < 1
    error('Output file must be jpg, gif, png, or bmp');
end
if isempty(dicom_list)
    error('You need at least one dicom file');
end
n = numrows(dicom_list);
t = dicomread(dicom_list{1});
if ndims(t) == 3
    X = nan(numrows(t), numcols(t), n);
    X(:, :, 1) = t;
    for ii = 2 : n
        this_data = double(dicomread(dicom_list{ii}));
        if ~(all(size(this_data) == size(t)))
            X(:, :, ii) = []; % e.g. last scan could be partial data
            continue
        else
            X(:, :, ii) = this_data;
        end
    end
    mean_X = uint8(interpolate(min(X(:)), max(X(:)), 0, 255, mean(X, 3)));
    [~, ~, ext] = fileparts(out_file);
    ext = lower(ext);
    if strcmpi(ext, '.jpg') || strcmp(ext, '.jpeg')
        imwrite(mean_X, out_file, 'jpeg', 'Quality',100);
    elseif strcmpi(ext, '.gif')
        imwrite(mean_X, out_file, 'gif');
    elseif strcmpi(ext, '.png')
        imwrite(mean_X, out_file, 'png');
    elseif strcmpi(ext, '.bmp')
        imwrite(mean_X, out_file, 'bmp');
    end
else
    assert(ndims(t) == 4);
    X = nan(size(t,1), size(t,2), size(t,3), size(t,4), n);
    X(:, :, :, :, 1) = t;
    for ii = 2 : n
        this_data = double(dicomread(dicom_list{ii}));
        if ~(all(size(this_data) == size(t)))
            X(:, :, :, :, ii) = []; % e.g. last scan could be partial data
            continue
        else
            X(:, :, :, :, ii) = this_data;
        end
    end
    mean_X = uint8(interpolate(min(X(:)), max(X(:)), 0, 255, mean(X, 5)));
    mean_X = squeeze(mean_X);
    for ii = 1 : 6 : size(mean_X, 3)
        try
            [base_dir, base_name, ext] = fileparts(out_file);
            ext = lower(ext);
            if strcmpi(ext, '.jpg') || strcmp(ext, '.jpeg')
                imwrite(mean_X(:, :, ii), fullfile(base_dir, ...
                    [sprintf('slice_%d',ii) base_name ext]), 'jpeg', 'Quality',100);
            elseif strcmpi(ext, '.gif')
                imwrite(mean_X(:, :, ii), fullfile(base_dir, ...
                    [sprintf('slice_%d',ii) base_name ext]), 'gif');
            elseif strcmpi(ext, '.png')
                imwrite(mean_X(:, :, ii), fullfile(base_dir, ...
                    [sprintf('slice_%d',ii) base_name ext]), 'png');
            elseif strcmpi(ext, '.bmp')
                imwrite(mean_X(:, :, ii), fullfile(base_dir, ...
                    [sprintf('slice_%d',ii) base_name ext]), 'bmp');
            end
        catch ME
            disp(ME)
        end
    end
end


end

function x = interpolate(orig_min, orig_max, new_min, new_max, data)
x = ((data - orig_min) .* (new_max - new_min)) ./ (orig_max - orig_min) + new_min;
end

