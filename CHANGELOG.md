# Changelog

## 1.1.1
### Fixed
- DICOM movies now display with correct range of values
DICOMS are in uint16, but videoWrite compatibility, I was interpolating to uint8 (8 bit).
This worked fine as long as the values were not too spread out. But DICOMS exported from
XA30 have more extreme values, and so a lot of information got lost. However, I was able to 
get around this problem by converting the values to floats, and then dividing by the maximum,
so in range [0,1].

### Changed
- QA output is kept in a separate directory now.
In some cases, the QA nifti output was being confused for SPM output, causing failure for some script routines
when re-running with "qa_only"


### TODO
- Mean functional png files and DICOM movies are 1-slice only. More useful to show a grid of slices instead.

## 1.1.0
### Added
- Support for XA30 DICOM images
- A change log
### Fixed
- Fixed subtle prefix tracking bug (rare)
- Fixed subtle bug with 'qa_only' option
- File permission weirdness
- Checks if all functionals are the same dimensions
New scanner software exports last partial scan too. Interferes with unwarping.

## Version 1.0.1
Whatever was before this.
