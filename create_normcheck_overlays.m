function create_normcheck_overlays(output_dir, normalized_structural, mean_normalized_functional, outsh)
% CREATE_NORMCHECK_OVERLAYS  Creates normalization check overlay our
% saves a script that can be called to create that overlay.
%
%    OUTPUT_DIR <1xn char array>: Directory where you want the output image or script to go.
%    Typically this will be the a directory of preprocessed images.
%
%    NORMALIZED_STRUCTURAL <1xn char array>: Path to the subject's normalized structural.
%    
%    MEAN_NORMALIZED_FUNCTIONAL <1xn char array>: Path to a representative of the normalized
%    functional images being evaluated. This can be created using the
%    create_nifti_mean_image function.
%
%    OUTSH <boolean>: If true, instead of creating the norm-check overlay
%    directly using the system command, a bash script will be saved to
%    OUTPUT_DIR/normoverlay.sh. This is a work around for a XWindow/MATLAB 
%    bug encountered in some environments where calling the script from 
%    MATLAB fails, but calling it outside of MATLAB succeeds.
%
% Currently the overlays are created using FSLEyes.
%
[script_dir, ~, ~] = fileparts(mfilename('fullpath'));
command_string = sprintf(...
        'export PATH="%s:$PATH"; /usr/bin/env bash create_normcheck_overlays.sh "%s" "%s" "%s"', ...
        script_dir, output_dir, normalized_structural, mean_normalized_functional);
if outsh
    try
        outfilestr = fullfile(output_dir, 'normoverlay.sh');
        fid = fopen(fullfile(output_dir, 'normoverlay.sh'), 'w');
        fprintf(fid, '%s\n', command_string);
        fclose(fid);
    catch ME
        warning('Could not create file %s\nwith error %s: %s', ...
            outfilestr, ME.identifier, ME.message);
        if exist('fid', 'var') == 1
            fclose(fid);
        end
    end
else
    [status, result] = system(command_string);
    if status
       warning(['System call to create normcheck overlay ' ...
           'returned a non-zero exit status with eeror message: %s'], result);
    end
end
end