function [TR, slice_times, nslices, TA, ...
    slice_order, reference_slice] = get_slice_timing(functional_dicom_filename)
d = dicominfo(functional_dicom_filename);
if isfield(d, 'SoftwareVersions') && contains(d.SoftwareVersions, 'XA30') 
    [TR, slice_times, nslices, TA, ...
        slice_order, reference_slice] = xa30_get_slice_timing(d);
elseif isfield(d, 'SoftwareVersion') && contains(d.SoftwareVersion, 'XA30')
    [TR, slice_times, nslices, TA, ...
        slice_order, reference_slice] = xa30_get_slice_timing(d);
else
        
    [TR, slice_times, nslices, TA, ...
        slice_order, reference_slice] = trio_get_slice_timing(d);
end
end


function [TR, slice_times, nslices, TA, ...
    slice_order, reference_slice] = trio_get_slice_timing(hdr)
TR = round(hdr.RepetitionTime / 1000);
slice_times = hdr.Private_0019_1029;
nslices = length(slice_times);
TA = TR - (TR / nslices);
[~, slice_order] = sort(slice_times);
reference_slice = slice_order(round(nslices/2));
end

function [TR, slice_times, nslices, TA, ...
    slice_order, reference_slice] = xa30_get_slice_timing(hdr)
repetition_time = hdr.SharedFunctionalGroupsSequence.Item_1.MRTimingAndRelatedParametersSequence.Item_1.RepetitionTime;
TR = round(repetition_time / 1000);
nslices = hdr.NumberOfFrames;
TA = TR - (TR / nslices);
struct_fields = cell(nslices, 1);
% Create a list of structure fields for each slice
for ii = 1 : nslices
    struct_fields{ii} = sprintf('Item_%d', ii);
end
slice_times = nan(nslices, 1);
for ii = 1 : length(struct_fields)
    disp(struct_fields{ii});
    try
        aqt = hdr.PerFrameFunctionalGroupsSequence.(struct_fields{ii}).FrameContentSequence.Item_1.FrameAcquisitionDatetime;
    catch
        aqt = hdr.PerFrameFunctionalGroupsSequence.(struct_fields{ii}).FrameContentSequence.Item_1.FrameAcquisitionDateTime;
    end
    slice_times(ii) = str2num(aqt);
end
[~, slice_order] = sort(slice_times);
reference_slice = slice_order(round(nslices/2));
end