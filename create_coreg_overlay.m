function create_coreg_overlay(structural_image, functional_image, outsh, out_dir, out_filename, cutoff)
% CREATE_COREG_OVERLAY  Creates coregistration overlay or saves a script
% that can be called to create that overlay.
%
%    STRUCTURAL_IMAGE <1xn char array>: The subject's post-coregistration
%    structural.
%
%    FUNCTIONAL_IMAGE <1xn char array>: A functional image. Usually the
%    mean functional image produced after co-registration.
%
%    CUTOFF <1x1 matrix>: Between 0 and 1, a quantile used to threshold
%    functional image for overlay. Defaults to 0.75.
%
%    OUTSH <boolean>: If true, instead of creating the norm-check overlay
%    directly using the system command, a bash script will be saved to
%    OUTPUT_DIR/normoverlay.sh. This is a work around for a XWindow/MATLAB
%    bug encountered in some environments where calling the script from
%    MATLAB fails, but calling it outside of MATLAB succeeds.
%
%
%
% Currently the overlays are created using FSLEyes.
%

if nargin < 6
    cutoff = 0.75;
end
[script_dir, ~, ~] = fileparts(mfilename('fullpath'));
functional_volume = spm_vol(functional_image);
X = spm_read_vols(functional_volume);
X = X(:);
Q = quantile(X, cutoff);
command_string = sprintf('export PATH="%s:$PATH"; /usr/bin/env bash create_coreg_overlay.sh "%s" "%s" "%s" "%s"', ...
    script_dir, structural_image, functional_image, Q, fullfile(out_dir, out_filename));
if outsh
    try
        outfilestr = fullfile(out_dir, 'local-create_coreg_overlay.sh');
        fid = fopen(outfilestr, 'w');
        fprintf(fid, '%s\n', command_string);
        fclose(fid);
    catch ME
        warning('Could not create file %s\nwith error %s: %s', ...
            outfilestr, ME.identifier, ME.message);
        if exist('fid', 'var') == 1
            fclose(fid);
        end
    end
else
    % This fails right now on the vm with an obscure x window error.
    % TODO: check whether it fails silently (i.e with zero status)
    [status, result] = system(command_string);
    if status
        warning(['System call to create coregistration overlay ' ...
            'returned a non-zero exit status with eeror message: %s'], result);
    end
end
end
