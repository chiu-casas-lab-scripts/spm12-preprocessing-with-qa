function auto_reorient_structural(p)
% AUTO_REORIENT_STRUCTURAL  Trivial wrapper around auto_reorient.m
%    P <1xn char array>: Path to structural image to reorient.
%
% This is slightly silly. But this makes intended use-case plain.
auto_reorient(p);
end