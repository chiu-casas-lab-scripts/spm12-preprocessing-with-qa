#! /usr/bin/env bash

structural=$1
functional=$2
cutoff=$3
outfile=$4
command -v fsleyes >/dev/null 2>&1 || { source paths.sh; }

echo "outfile=$outfile"
echo "structural=$structural"
echo "functional=$functional"
echo "cutoff=$cutoff"


worldLoc='-9.925351299926135 25.625554442300626 15.788999852296229'
/usr/bin/env fsleyes render --outfile "$outfile"  --scene ortho --neuroOrientation \
        --worldLoc $worldLoc \
        --displaySpace "$structural" \
        --xcentre -0.01364 -0.04780 --ycentre -0.04361 -0.04780 --zcentre -0.04361 -0.01364 \
        --xzoom 421.4204736952965 --yzoom 578.3987700837185 --zzoom 578.3987700837185 \
        --layout horizontal --bgColour 0.0 0.0 0.0 --fgColour 1.0 1.0 1.0 --cursorColour 0.0 1.0 0.0 \
        --colourBarLocation top --colourBarLabelSide top-left --colourBarSize 100.0 \
        --labelSize 12 --performance 3 --movieSync "$structural" \
        --overlayType volume --alpha 100.0 --brightness 50.0 --contrast 50.0 --cmap greyscale \
        --negativeCmap greyscale --displayRange 0.0 958.0 --clippingRange 0.0 967.58\
        --gamma 0.0 --cmapResolution 256 --interpolation none --numSteps 100 --blendFactor 0.1 \
        --smoothing 0 --resolution 100 --numInnerSteps 10 --clipMode intersection --volume 0 \
        "$functional" --overlayType volume --alpha 50.0 --cmap red-yellow --negativeCmap greyscale --useNegativeCmap \
        --displayRange 0.0 1451.278 \
        --clippingRange "$cutoff" 1465.7906103515625 \
        --gamma 0.0 --cmapResolution 256 --interpolation none --numSteps 100 \
        --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 --clipMode intersection --volume 0
