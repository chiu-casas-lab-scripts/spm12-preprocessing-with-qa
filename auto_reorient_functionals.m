function auto_reorient_functionals(reference, scans)
% AUTO_REORIENT_FUNCTIONALS  Auto-reorients reference scan and applies 
% transform to remaining list of scans.
%
%     REFERENCE <1xn char array>:  A functional scan that whose adjusted 
%       orientation matrix will be copied. Typically the first scan.
%
%     SCANS <nx1 cell array>: List of functional scans to be reoriented 
%       using transformation matrix from reference.
%
% Example usage:
%
%    functional_scans = cellstr(spm_select('FPList', func_dir, '^f.*\.nii$'));
%    auto_reorient_functionals(functional_scans{1}, functional_scans);
%
% The reference scan is automatically removed from SCANS before applying
% the transformation matrix. It will also attempt to remove duplicates from
% the list of scans.
%
% If we run script on every functional, we would have no meaningfull 
% realignment parameters! That is why we apply auto_reorient only to the
% reference scan.
%

old = spm_get_space(reference);
auto_reorient(reference);
new = spm_get_space(reference);
x_matrix = new / old;
% Remove reference from list of scans
scans = scans(...
    ~cellfun(@(s)strcmp(s, reference), scans));
N = numel(scans);
% Two state operation to avoid applying transform twice if duplicates in
% list of scans. spm_get_space actually is not *just* a read operation, but
% if given a second argument, changes the volume space of the file.
M = zeros(4, 4, N);
for ii = 1 : N
    M(:, :, ii) = spm_get_space(scans{ii});
end
for ii = 1 : N
    spm_get_space(scans{ii}, x_matrix * M(:, :, ii));
end
end