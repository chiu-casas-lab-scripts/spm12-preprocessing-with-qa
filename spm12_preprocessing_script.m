function spm12_preprocessing_script(dicom_dir, task_id, out_dir, options)
%STEP_PREPROCESSING_SPM12
% An over-engineered preprocessing script for SPM12. Its designed to
% handle some most common preprocessing use-cases with little fuss.
% Arguments:
%   dicom_dir: the root directory path of all your dicom files. It is assumed
%   that data is organized as follows:
%       dicom_dir
%       |_TaskID1
%       | |_structural
%       | |_functional
%       |_TaskID2
%       ...
%   with all structural ima's and functional ima's in the respective
%   directories.
%
%  task_id: the task identifier for each subject's run. Must match the name
%  of the folder (TaskID1, etc.) in dicom_dir
%  
%  out_dir: the root directory path in which the scripts output will be
%  put. Preprocessed data will be output as follows:
%       output_dir
%       |_TaskID1
%       | |_structural
%       | |_functional
%       |_TaskID2
%       ...
%
%  options: A cell with string arguments on which processing steps to run.
%  - {'typical'} runs pipeline: dicom_import -> realign_images ->
%                slice_timing -> coregister -> normalize -> smooth
%  - {'typical_except_import'} is realign_images -> slice_timing -> 
%                coregister -> normalize -> smooth
%  - {'import_only'} runs pipeline: dicom_import. Does not auto-reorient.
%  - {'import_only_with_auto_reorient'} runs dicom_import but applies
%                 auto-reorient.
%  - {'with_unwarp'} is equivalent to {'typical'} except that the unwarp
%                routine is run after realignment.
%  - {'with_unwarp_except_import'} is equivalent to {'with_unwarp'} except
%                dicoms are not imported.
%  Including more than one of these options in the cell array of options
%  will result in unintended behavior.
%
%  Additional options can also be specified by adding them to the cell
%  array of options. These include:
%  - { ... 'no_auto_reorient'} import data without auto-reorient being
%                    applied. Does nothing if not importing.
%  - { ... 'no_qa'}  will stop the quality assurance routines from running.
%                    This option overrides the 'outsh' and 'wilkes'
%                    options.
%  - { ... 'outsh'}  will cause FSL/XWindow dependent routines from running,
%                    but will instead write to a bash file to be run later.
%  - { ... 'wilkes'} if you want to run Marko Wilkes motion fingerprint. To
%                    do so you will need to put mw_mfp.m and mw_anamot.m on 
%                    the MATLAB path. These .m files can be obtained by 
%                    downloading the Motion Fingerprint software from
%                    here: https://www.medizin.uni-tuebingen.de/de/das-klinikum/einrichtungen/kliniken/kinderklinik/kinderheilkunde-iii/forschung-iii/software
%
%                   
%  You can modify the script to add more options.
%
%  This function outputs, for each stage, a batch mat file in the subject's
%  root preprocessing directory. If you need to tweak parameters, you can
%  modify that batch file and run it directly spm_jobman('run',
%  'batch.mat');
%
% Some of the parameter choices are not SPM defaults. In most cases, I have
% opted for better quality over performance since compute time is not
% really a major constraint anymore.
%
% The realignment step also calculates frame displacement and constructs a
% censor matrix using FD >= 0.9mm as the threshold. This can be used to
% censor high motion scans in your first level analyses.
% See: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3895106

% Sanitise args
dicom_dir = num2str(dicom_dir); % num2str converts [] to ''
task_id = num2str(task_id);     % and numbers to their string representations, 
out_dir = num2str(out_dir);     % but leaves strings just as they are.

% Saniy check arguments
if strcmp(dicom_dir, '')
    error('Argument `dicom_dir` should not be the empty string');
end
if strcmp(task_id, '')
    error('Argument `task_id` should not be the empty string');
end
if strcmp(out_dir, '')
    error('Argument `out_dir` should not be the empty string');
end 
if not(iscell(options)) 
    error('Argument `options` should be a cell array of option strings')
end

% Check spm
if ~exist('spm', 'file')
    error('spm is not on path!')
end
version_spm = spm('version');
if ~strcmp(version_spm(1:5), 'SPM12')
    error('Not using SPM12!');
end

% Time to check options.
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set flags to defaults
dicom_import =        false;
realign_images =      false;
unwarp_images =       false;
slice_timing =        false;
coregister =          false;
normalize =           false;
smooth =              false;
qa =                  true;
segment_separately =  false;
outsh =               false;
auto_reorient =       true;
wilkes =              false;

% Options must have one and only one of the following:
required_opts = {'typical', 'typical_except_import', ...
    'import_only', 'import_only_with_auto_reorient', ...
    'with_unwarp', 'with_unwarp_except_import', 'only_qa'};
num_required_opts = 0;
for ii = 1 : length(required_opts)
   num_required_opts = num_required_opts + ismember(required_opts{ii}, options);
end
if num_required_opts ~= 1
    error('One and only one required option can be selected from {%s}.', ...
        strjoin(required_opts, ', '));
end

% Each required option sets up a unique preprocessing pipeline
% If you want to change the order of pipeline options, then you'll have
% to do more fancy conditional footwork.
if ismember('typical', options)
    dicom_import = true;
    realign_images = true;
    slice_timing = true;
    coregister = true;
    segment_separately = true;
    normalize = true;
    smooth = true;
elseif ismember('typical_except_import', options)
    realign_images = true;
    slice_timing = true;
    coregister = true;
    segment_separately = true;
    normalize = true;
    smooth = true;
elseif ismember('import_only', options)
    dicom_import = true;
    auto_reorient = false;
    qa = false;
elseif ismember('import_only_with_auto_reorient', options)
    dicom_import = true;
    auto_reorient = true;
    qa = false;
elseif ismember('with_unwarp', options)
    dicom_import = true;
    realign_images = true;
    unwarp_images = true;
    slice_timing = true;
    coregister = true;
    segment_separately = true;
    normalize = true;
    smooth = true;
elseif ismember('with_unwarp_except_import', options)
    realign_images = true;
    unwarp_images = true;
    slice_timing = true;
    segment_separately = true;
    coregister = true;
    normalize = true;
    smooth = true;
elseif ismember('only_qa', options)
    %only_qa = true; % already default
    auto_reorient = false; % not strictly necessary
else
    % This is guarded against, but we'll keep it here to satisfy paranoia.
    warning('No processing pipeline specified!');
    return;
end

% Optional options (sic)
if ismember('no_auto_reorient', options)
    auto_reorient = false;
end
if ismember('wilkes', options)
    wilkes = true;
end
if ismember('no_qa', options)
    % if both only_qa and no_qa, we'll just take the caller at their word.
    qa = false;
end
if ismember('outsh', options)
    % hacks the qa to write scripts instead
    outsh = true;
end



% Set Paths and initialize SPM
[script_dir, ~, ~] = fileparts(mfilename('fullpath'));
path_to_spm = which('spm');
[dir_spm, ~, ~] = fileparts(path_to_spm);
spm('defaults', 'FMRI');
spm_jobman('initcfg');

source_task_dir = fullfile(dicom_dir, task_id);
source_task_dir_functional = fullfile(source_task_dir, 'functional');
source_task_dir_structural = fullfile(source_task_dir, 'structural');
    
% Create target directories if they do not exist
target_task_dir = fullfile(out_dir, task_id);
target_task_dir_functional = fullfile(target_task_dir, 'functional');
target_task_dir_structural = fullfile(target_task_dir, 'structural');

qa_dir = fullfile(target_task_dir, 'qa');

if ~exist(target_task_dir, 'dir')
    mkdir(target_task_dir);
end
if ~exist(target_task_dir_functional, 'dir')
    mkdir(target_task_dir_functional);
end
if ~exist(target_task_dir_structural, 'dir')
    mkdir(target_task_dir_structural);
end
if ~exist(qa_dir, 'dir')
   mkdir(qa_dir); 
end

% Lets boogie
fprintf('Not all who wander are lost ... \n');
fprintf('Preprocessing task %s ...\n', task_id);

% This forces SPM12 to create the graphics; 
% https://www.jiscmail.ac.uk/cgi-bin/webadmin?A2=spm;99b12bbc.1711
fig_handle = spm_figure('GetWin','Graphics');

if dicom_import
    clear('matlabbatch');
    
    fprintf('Begin import ...')
    if ~exist(source_task_dir, 'dir')
        warning('Source dicom directory %s does not exist!\n', source_task_dir);
        return; % Raising an error may cluster job to fail
    end
    
    % Specify batch for functional images
    functional_imgs = fetch_functional_dicoms(source_task_dir_functional);
    
    matlabbatch{1}.spm.util.import.dicom.data = functional_imgs;
    matlabbatch{1}.spm.util.import.dicom.root = 'flat';
    matlabbatch{1}.spm.util.import.dicom.outdir = {target_task_dir_functional};
    matlabbatch{1}.spm.util.import.dicom.protfilter = '.*';
    matlabbatch{1}.spm.util.import.dicom.convopts.format = 'nii';
    matlabbatch{1}.spm.util.import.dicom.convopts.meta = 0;
    matlabbatch{1}.spm.util.import.dicom.convopts.icedims = 0;
    
    % Specify batch for structural images
    structural_imgs = cellstr(spm_select('FPList', ...
        source_task_dir_structural, '^.*\.ima$'));
    matlabbatch{2}.spm.util.import.dicom.data = structural_imgs;
    matlabbatch{2}.spm.util.import.dicom.root = 'flat';
    matlabbatch{2}.spm.util.import.dicom.outdir = {target_task_dir_structural};
    matlabbatch{2}.spm.util.import.dicom.protfilter = '.*';
    matlabbatch{2}.spm.util.import.dicom.convopts.format = 'nii';
    matlabbatch{2}.spm.util.import.dicom.convopts.meta = 0;
    matlabbatch{2}.spm.util.import.dicom.convopts.icedims = 0;
    
    % Run
    save(fullfile(target_task_dir, 'dicom_import_batch.mat'), ...
        'matlabbatch'); % Record keeping for posterity
    spm_jobman('run', matlabbatch);
    fprintf('Import of %s complete!\n', task_id);

    if auto_reorient
        
        % Now we run auto reorient on the structural.
        sub_struct = spm_select('FPList', ...
            target_task_dir_structural, '^s.*\.nii$');
        if isempty(sub_struct)
            % Could be a multi-frame dicom, e.g. new XA30
            sub_struct = spm_select('FPList', ...
                target_task_dir_structural, '^MF.*\.nii$');
        end
        sub_struct = cellstr(sub_struct);
        auto_reorient_structural(sub_struct);
        
        % Now functionals
        target_funct = spm_select('FPList', ...
            target_task_dir_functional, '^f.*\.nii$');
        if isempty(target_funct)
            % Could be a multi-frame dicom, e.g. new XA30
            target_funct = spm_select('FPList', ...
                target_task_dir_functional, '^MF.*\.nii$');
        end
        target_funct = cellstr(target_funct);
        auto_reorient_functionals(target_funct{1}, target_funct);
    end
end

% multi-frame functional dicoms imported with 'MF', mosaics with 'f'
if isempty(spm_select('FPList', target_task_dir_functional, '^f.*\.nii$'))
    running_functional_prefix = 'MF';
else
    running_functional_prefix = 'f';
end

if isempty(spm_select('FPList', target_task_dir_structural, '^s.*\.nii$'))
    structural_prefix = 'MF';
else
    structural_prefix = 's';
end

if realign_images && ~unwarp_images
    clear('matlabbatch');
    
    fprintf('Begin realignment and reslice of %s ...\n', task_id);
    realign_functional = cellstr(spm_select('FPList', ...
        target_task_dir_functional, ...
        ['^' running_functional_prefix '.*\.nii$'] ...
        ));
    
    matlabbatch{1}.spm.spatial.realign.estwrite.data = {realign_functional}; % if multiple sessions, may need to revise
    matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.quality = 1; % not default
    matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.sep = 3; % not default
    matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.fwhm = 5;
    matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.rtm = 1;
    matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.interp = 6; % not default
    matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.wrap = [0 1 0];
    matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.weight = '';
    matlabbatch{1}.spm.spatial.realign.estwrite.roptions.which = [2 1];
    matlabbatch{1}.spm.spatial.realign.estwrite.roptions.interp = 6; % not default
    matlabbatch{1}.spm.spatial.realign.estwrite.roptions.wrap = [0 0 0];
    matlabbatch{1}.spm.spatial.realign.estwrite.roptions.mask = 1;
    matlabbatch{1}.spm.spatial.realign.estwrite.roptions.prefix = 'r';
    
    save(fullfile(target_task_dir, 'realign_batch.mat'), ...
        'matlabbatch'); % Record keeping for posterity
    spm_jobman('run', matlabbatch);
    
    % Obtain Powers et al. Frame Displacment (FD) and FD censor matrix
    [realignment_param_file, ~] = spm_select('FPList', ...
        target_task_dir_functional, '^rp_.*\.txt');
    realignment_params = load(realignment_param_file);
    fd = calcFD(realignment_params);
    fd_threshold = 0.9; % mm; see https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3895106/ and 
                        % and http://mvpa.blogspot.com/2017/05/task-fmri-motion-censoring-scrubbing-2.html
    fd_censor_matrix = buildCensorMatrix(fd, fd_threshold);
    save(fullfile(target_task_dir_functional, 'fd_regressors.mat'), ...
        'fd_censor_matrix', 'fd_threshold', 'fd', 'realignment_params');
    h = drawFD(fd, fd_threshold);
    saveas(h, fullfile(target_task_dir_functional, 'fd_plot.fig'), 'fig');
    clear('h');
    fprintf('Realignment of %s complete!\n', task_id);
    running_functional_prefix = 'r';
end

% Unwarp should only be done if necessary .
if realign_images && unwarp_images
    clear('matlabbatch');
    fprintf('Begin realignment , reslice, and unwarping of %s ...\n', task_id);
    expr =  ['^' running_functional_prefix '.*\.nii$'];
    
    realign_functional = cellstr(spm_select('FPList', ...
        target_task_dir_functional, expr));
    
    matlabbatch{1}.spm.spatial.realignunwarp.data.scans = realign_functional;
    matlabbatch{1}.spm.spatial.realignunwarp.data.pmscan = '';
    matlabbatch{1}.spm.spatial.realignunwarp.eoptions.quality = 1; % not default
    matlabbatch{1}.spm.spatial.realignunwarp.eoptions.sep = 3; % not default
    matlabbatch{1}.spm.spatial.realignunwarp.eoptions.fwhm = 5;
    matlabbatch{1}.spm.spatial.realignunwarp.eoptions.rtm = 0;
    matlabbatch{1}.spm.spatial.realignunwarp.eoptions.einterp = 6; % not default
    matlabbatch{1}.spm.spatial.realignunwarp.eoptions.ewrap = [0 1 0]; % wrap in y direction
    matlabbatch{1}.spm.spatial.realignunwarp.eoptions.weight = '';
    matlabbatch{1}.spm.spatial.realignunwarp.uweoptions.basfcn = [12 12];
    matlabbatch{1}.spm.spatial.realignunwarp.uweoptions.regorder = 1;
    matlabbatch{1}.spm.spatial.realignunwarp.uweoptions.lambda = 100000;
    matlabbatch{1}.spm.spatial.realignunwarp.uweoptions.jm = 0;
    matlabbatch{1}.spm.spatial.realignunwarp.uweoptions.fot = [4 5];
    matlabbatch{1}.spm.spatial.realignunwarp.uweoptions.sot = [];
    matlabbatch{1}.spm.spatial.realignunwarp.uweoptions.uwfwhm = 4;
    matlabbatch{1}.spm.spatial.realignunwarp.uweoptions.rem = 1;
    matlabbatch{1}.spm.spatial.realignunwarp.uweoptions.noi = 5;
    matlabbatch{1}.spm.spatial.realignunwarp.uweoptions.expround = 'Average';
    matlabbatch{1}.spm.spatial.realignunwarp.uwroptions.uwwhich = [2 1];
    matlabbatch{1}.spm.spatial.realignunwarp.uwroptions.rinterp = 6; % not default
    matlabbatch{1}.spm.spatial.realignunwarp.uwroptions.wrap = [0 0 0];
    matlabbatch{1}.spm.spatial.realignunwarp.uwroptions.mask = 1;
    matlabbatch{1}.spm.spatial.realignunwarp.uwroptions.prefix = 'u';

    save(fullfile(target_task_dir, 'realign_batch.mat'), ...
        'matlabbatch'); % Record keeping for posterity
    spm_jobman('run', matlabbatch);
    
    % Obtain Powers et al. Frame Displacment (FD) and FD censor matrix
    [realignment_param_file, ~] = spm_select('FPList', ...
        target_task_dir_functional, '^rp_.*\.txt');
    realignment_params = load(realignment_param_file);
    fd = calcFD(realignment_params);
    fd_threshold = 0.9; % mm; see https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3895106/ and 
                        % and http://mvpa.blogspot.com/2017/05/task-fmri-motion-censoring-scrubbing-2.html
    fd_censor_matrix = buildCensorMatrix(fd, fd_threshold);
    save(fullfile(target_task_dir_functional, 'fd_regressors.mat'), ...
        'fd_censor_matrix', 'fd_threshold', 'fd', 'realignment_params');
    h = drawFD(fd, fd_threshold);
    saveas(h, fullfile(target_task_dir_functional, 'fd_plot.fig'), 'fig');
    clear('h');
    fprintf('Realignment of %s complete!\n', task_id);
    running_functional_prefix = 'u';
end


if slice_timing
    clear('matlabbatch');
    
    % If dicoms acquired on PRISMA or, especially a non-Siemens scanner, 
    % you may need to revisit how these parameters are determined.
    dicom_files = dir(fullfile(source_task_dir_functional, '*ima'));
    expr = ['^' running_functional_prefix '.*\.nii$'];
    functional_imgs = cellstr(...
        spm_select('FPList', target_task_dir_functional, expr));
    [TR, slice_times, nslices, TA, ...
        slice_order, reference_slice] = get_slice_timing(...
        fullfile(source_task_dir_functional, dicom_files(1).name));
    
    matlabbatch{1}.spm.temporal.st.scans = {functional_imgs}; % Need to revisit if mulitple sessions
    matlabbatch{1}.spm.temporal.st.nslices = nslices;
    matlabbatch{1}.spm.temporal.st.tr = TR;
    matlabbatch{1}.spm.temporal.st.ta = TA;
    matlabbatch{1}.spm.temporal.st.so = slice_order;
    matlabbatch{1}.spm.temporal.st.refslice = reference_slice;
    matlabbatch{1}.spm.temporal.st.prefix = 'a';
    save(fullfile(target_task_dir, 'reslice_batch.mat'), ...
        'matlabbatch'); % Record keeping for posterity
    spm_jobman('run', matlabbatch);
    running_functional_prefix = 'a';
end

if coregister
    clear('matlabbatch');
    
    fprintf('Begin coregistration of %s ...\n', task_id);
    mean_func_img = cellstr(spm_select('FPList', ...
        target_task_dir_functional, '^mean.*\.nii$'));
    sub_mprage = cellstr(spm_select('FPList', ...
        target_task_dir_structural, ['^' structural_prefix '.*\.nii$']));
    matlabbatch{1}.spm.spatial.coreg.estimate.ref = mean_func_img;
    matlabbatch{1}.spm.spatial.coreg.estimate.source = sub_mprage;
    matlabbatch{1}.spm.spatial.coreg.estimate.other = {''};
    matlabbatch{1}.spm.spatial.coreg.estimate.eoptions.cost_fun = 'nmi';
    matlabbatch{1}.spm.spatial.coreg.estimate.eoptions.sep = [4 2];
    matlabbatch{1}.spm.spatial.coreg.estimate.eoptions.tol = [0.02 0.02 ...
        0.02 0.001 0.001 0.001 0.01 0.01 0.01 0.001 0.001 0.001];
    matlabbatch{1}.spm.spatial.coreg.estimate.eoptions.fwhm = [7 7];
    save(fullfile(target_task_dir, 'coregistration_batch.mat'), 'matlabbatch');
    spm_jobman('run', matlabbatch);
    saveas(fig_handle, fullfile(target_task_dir, 'histogram.fig'))
    fprintf('Coregistration of %s complete!', task_id);
end

if segment_separately
    % We'll do this so that we can get white/gray masks in subject_space,
    % which might be useful for various purposes; otherwise we use the new
    % segment/normalize routine for actually getting into MNI space.
    % This should be done after co-registration, so that they aligns
    clear('matlabbatch');
    fprintf('Segment and create tissues masks in subject space ...');
    sub_mprage = spm_select('FPList', ...
        target_task_dir_structural, ['^' structural_prefix '.*\.nii$']);
    sub_mprage = cellstr(sub_mprage);
    matlabbatch{1}.spm.spatial.preproc.channel.vols = sub_mprage;
    matlabbatch{1}.spm.spatial.preproc.channel.biasreg = 0.001;
    matlabbatch{1}.spm.spatial.preproc.channel.biasfwhm = 60;
    matlabbatch{1}.spm.spatial.preproc.channel.write = [0 0];
    matlabbatch{1}.spm.spatial.preproc.tissue(1).tpm = {fullfile(spm('Dir'), 'tpm/TPM.nii,1')};
    matlabbatch{1}.spm.spatial.preproc.tissue(1).ngaus = 1;
    matlabbatch{1}.spm.spatial.preproc.tissue(1).native = [1 0];
    matlabbatch{1}.spm.spatial.preproc.tissue(1).warped = [0 0];
    matlabbatch{1}.spm.spatial.preproc.tissue(2).tpm = {fullfile(spm('Dir'), 'tpm/TPM.nii,2')};
    matlabbatch{1}.spm.spatial.preproc.tissue(2).ngaus = 1;
    matlabbatch{1}.spm.spatial.preproc.tissue(2).native = [1 0];
    matlabbatch{1}.spm.spatial.preproc.tissue(2).warped = [0 0];
    matlabbatch{1}.spm.spatial.preproc.tissue(3).tpm = {fullfile(spm('Dir'), 'tpm/TPM.nii,3')};
    matlabbatch{1}.spm.spatial.preproc.tissue(3).ngaus = 2;
    matlabbatch{1}.spm.spatial.preproc.tissue(3).native = [1 0];
    matlabbatch{1}.spm.spatial.preproc.tissue(3).warped = [0 0];
    matlabbatch{1}.spm.spatial.preproc.tissue(4).tpm = {fullfile(spm('Dir'), 'tpm/TPM.nii,4')};
    matlabbatch{1}.spm.spatial.preproc.tissue(4).ngaus = 3;
    matlabbatch{1}.spm.spatial.preproc.tissue(4).native = [1 0];
    matlabbatch{1}.spm.spatial.preproc.tissue(4).warped = [0 0];
    matlabbatch{1}.spm.spatial.preproc.tissue(5).tpm = {fullfile(spm('Dir'), 'tpm/TPM.nii,5')};
    matlabbatch{1}.spm.spatial.preproc.tissue(5).ngaus = 4;
    matlabbatch{1}.spm.spatial.preproc.tissue(5).native = [1 0];
    matlabbatch{1}.spm.spatial.preproc.tissue(5).warped = [0 0];
    matlabbatch{1}.spm.spatial.preproc.tissue(6).tpm = {fullfile(spm('Dir'), 'tpm/TPM.nii,6')};
    matlabbatch{1}.spm.spatial.preproc.tissue(6).ngaus = 2;
    matlabbatch{1}.spm.spatial.preproc.tissue(6).native = [1 0];
    matlabbatch{1}.spm.spatial.preproc.tissue(6).warped = [0 0];
    matlabbatch{1}.spm.spatial.preproc.warp.mrf = 1;
    matlabbatch{1}.spm.spatial.preproc.warp.cleanup = 1;
    matlabbatch{1}.spm.spatial.preproc.warp.reg = [0 0.001 0.5 0.05 0.2];
    matlabbatch{1}.spm.spatial.preproc.warp.affreg = 'mni';
    matlabbatch{1}.spm.spatial.preproc.warp.fwhm = 0;
    matlabbatch{1}.spm.spatial.preproc.warp.samp = 1;
    matlabbatch{1}.spm.spatial.preproc.warp.write = [0 0];
    matlabbatch{1}.spm.spatial.preproc.warp.vox = NaN;
    matlabbatch{1}.spm.spatial.preproc.warp.bb = [NaN NaN NaN
        NaN NaN NaN];

end

if normalize
    clear('matlabbatch');
    fprintf('Begin normalization estimate of %s ...\n', task_id);
    structural = cellstr(spm_select('FPList', ...
        target_task_dir_structural, ['^' structural_prefix '.*\.nii$']));
    all_func_imgs = cellstr(spm_select('FPList', ...
        target_task_dir_functional, ['^' running_functional_prefix, '.*\.nii$'])); %sprintf('^%s.*\\.nii$', running_functional_prefix)));
    matlabbatch{1}.spm.spatial.normalise.est.subj.vol = structural;
    matlabbatch{1}.spm.spatial.normalise.est.eoptions.biasreg = 0.0001;
    matlabbatch{1}.spm.spatial.normalise.est.eoptions.biasfwhm = 60;
    matlabbatch{1}.spm.spatial.normalise.est.eoptions.tpm = {fullfile(...
        dir_spm, 'tpm/TPM.nii')};
    matlabbatch{1}.spm.spatial.normalise.est.eoptions.affreg = 'mni';
    matlabbatch{1}.spm.spatial.normalise.est.eoptions.reg = [0 0.001 0.5 0.05 0.2];
    matlabbatch{1}.spm.spatial.normalise.est.eoptions.fwhm = 0;
    matlabbatch{1}.spm.spatial.normalise.est.eoptions.samp = 2;
    save(fullfile(target_task_dir, 'normalization_estimate_batch.mat'), 'matlabbatch');
    spm_jobman('run', matlabbatch);
    
    % Now we've created the deformation field
    clear('matlabbatch');
    fprintf('Begin normalization write of %s ...\n', task_id);
    deformation_field = cellstr(spm_select('FPList', ...
        target_task_dir_structural, '^y_.*\.nii'));
    
    % Warp the functionals
    matlabbatch{1}.spm.spatial.normalise.write.subj.def = deformation_field;
    matlabbatch{1}.spm.spatial.normalise.write.subj.resample = all_func_imgs;
    matlabbatch{1}.spm.spatial.normalise.write.woptions.bb = [-78 -112 -70
        78 76 85];
    matlabbatch{1}.spm.spatial.normalise.write.woptions.vox = [3 3 3];
    matlabbatch{1}.spm.spatial.normalise.write.woptions.interp = 6;
    matlabbatch{1}.spm.spatial.normalise.write.woptions.prefix = 'w';
    
    % Warp the structural
    matlabbatch{2}.spm.spatial.normalise.write.subj.def = deformation_field;
    matlabbatch{2}.spm.spatial.normalise.write.subj.resample = structural;
    matlabbatch{2}.spm.spatial.normalise.write.woptions.bb = [-78 -112 -70
        78 76 85];
    matlabbatch{2}.spm.spatial.normalise.write.woptions.vox = [1 1 1];
    matlabbatch{2}.spm.spatial.normalise.write.woptions.interp = 6;
    matlabbatch{2}.spm.spatial.normalise.write.woptions.prefix = 'w';
    save(fullfile(target_task_dir, 'normalization_write_batch.mat'), 'matlabbatch');
    spm_jobman('run', matlabbatch);
    running_functional_prefix = 'w';
end


if smooth
    clear('matlabbatch');
    normalized_structural = cellstr(spm_select('FPList', ...
        target_task_dir_structural, '^w.*\.nii$'));
    norm_func_imgs = cellstr(spm_select('FPList', ...
        target_task_dir_functional, ['^' running_functional_prefix '.*\.nii'])); %sprintf('^%s.*\\.nii$', running_functional_prefix)))
    % The functionals
    matlabbatch{1}.spm.spatial.smooth.data = norm_func_imgs;
    matlabbatch{1}.spm.spatial.smooth.fwhm = [6 6 6]; % not default
    matlabbatch{1}.spm.spatial.smooth.dtype = 0;
    matlabbatch{1}.spm.spatial.smooth.im = 0;
    matlabbatch{1}.spm.spatial.smooth.prefix = 's';
    % The structural
    matlabbatch{2}.spm.spatial.smooth.data = normalized_structural;
    matlabbatch{2}.spm.spatial.smooth.fwhm = [2 2 2];
    matlabbatch{2}.spm.spatial.smooth.dtype = 0;
    matlabbatch{2}.spm.spatial.smooth.im = 0;
    matlabbatch{2}.spm.spatial.smooth.prefix = 'smoothed_';
    save(fullfile(target_task_dir, 'smoothing_batch.mat'), 'matlabbatch');
    spm_jobman('run', matlabbatch);
    running_functional_prefix = 's';
end

if qa
    % Mean DICOM functional and DICOM movie
    functional_dicoms = fetch_functional_dicoms(source_task_dir_functional);
    structural_dicoms = cellstr(spm_select('FPList', ...
        source_task_dir_structural, '^.*\.ima$'));
    create_dicom_mean_image(functional_dicoms, ...
        fullfile(qa_dir, [task_id 'mean_dicom_functional.png']), true);
    create_dicom_movie(functional_dicoms, ...
        fullfile(qa_dir, [task_id '-' 'functional_dicoms.avi']), true, 'functional');
    create_dicom_movie(structural_dicoms, ...
        fullfile(qa_dir, [task_id '-' 'structural_dicoms.avi']), true, 'structural');
    
    if wilkes
        [realignment_param_file, ~] = spm_select('FPList', ...
            target_task_dir_functional, '^rp_.*\.txt');
        mw_mfp(realignment_param_file);
        cd(script_dir);
        mw_anamot(target_task_dir_functional);
        cd(script_dir);
        saveas(gcf, fullfile(qa_dir, 'mw_anamot.fig'))
    end
    
    % Coregistration Overlay
    mean_coregistered_funct = cellstr(spm_select('FPList', ...
        target_task_dir_functional, '^mean.*\.nii$'));
    sub_struct = cellstr(spm_select('FPList', ...
        target_task_dir_structural, ['^' structural_prefix '.*\.nii$']));
    create_coreg_overlay(sub_struct{1}, mean_coregistered_funct{1}, outsh, ...
        qa_dir, [task_id '-' 'coreg_overlay.png']);
    
    % Normcheck Overlays
    running_functional_prefix = 's';
    normalized_structural = cellstr(spm_select('FPList', ...
        target_task_dir_structural, '^smoothed_w.*\.nii$'));
    normalized_functionals = spm_select('FPList', ...
        target_task_dir_functional, ...
        sprintf('^%s.*\\.nii$', running_functional_prefix));
    mean_funct_file = fullfile(qa_dir, ...
        [task_id '-' 'average_normalized_functional.nii']);
    create_nifti_mean_image(normalized_functionals, mean_funct_file, true);
    create_normcheck_overlays(qa_dir, normalized_structural{1}, ...
        mean_funct_file, outsh);
end
end

function fd_regressors = buildCensorMatrix(fd, fd_threshold)
% Builds nuisance regressor matrix for given frame displacement and threshold.
fd_regressors = [];
n = length(fd);
for ii = 1 : n
    if fd(ii) > fd_threshold
        disp(fd(ii))
        m = zeros(n, 1);
        m(ii, 1) = 1;
        fd_regressors(:, end+1) = m;
    end
end
end

function h = drawFD(fd, threshold)
n = length(fd);
X = 1:n;
h = figure;
hold on;
plot(X, fd, 'o-');
%stem(X, fd .* (fd > threshold))
line(X, repmat(threshold, n));
hold off
end

function functional_dicoms = fetch_functional_dicoms(functional_dicom_dir)
functional_dicoms = cellstr(spm_select('FPList', ...
    functional_dicom_dir, '^.*\.ima$'));
% The XA30 may export the final partial scan. This can be problematic
% as the dimensionality is different. We'll detect and remove it.
% Now functionals

t0 = dicomread(functional_dicoms{1});
tlast = dicomread(functional_dicoms{end});

if ~ (all(size(t0) == size(tlast)))
    functional_dicoms = functional_dicoms(1:end-1);
end
end

