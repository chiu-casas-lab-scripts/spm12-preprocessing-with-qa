function fd = calcFD(movement_parameters)
% Calculates Frame Displacement
sphere_radius = 50; % in mm
xyz = movement_parameters(:, [1,2,3]);
rot = movement_parameters(:, [4,5,6]);
% rot = deg2rad(rot); % This was a mistake. The parameters are already in radians
xyz_delta = abs(xyz(1:end-1, :) - xyz(2:end, :));
rot_delta = sphere_radius * (abs(rot(1:end-1, :) - rot(2:end, :)));
deltas = cat(2, xyz_delta, rot_delta);
fd = cat(1, 0, sum(deltas, 2));
end
