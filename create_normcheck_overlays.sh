alpha=10
colormap=red
colormap_alt=red
layout=horizontal
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
resource_dir="$script_dir"/resources
outputdir=$1
normalized_structural=$2
mean_functional=$3
if [ "$outputdir" == '' ]; then
    echo "No output directory specified"
    exit 1
fi
if [ "$normalized_structural" == '' ]; then
    echo "No normalized structural specified"
    exit 1
fi
if [ "$mean_functional" = '' ]; then
    echo "No mean normalized functional image specified"
    exit 1
fi

command -v fsleyes >/dev/null 2>&1 || { source paths.sh; }
#command -v fsleyes >/dev/null 2>&1 || { PATH=$PATH:/usr/local/fsl/bin; export PATH; }


# MNI -8 -20 12
# ###################################################################################
output_file="$outputdir"/norm_render_m08_m20_p12.png
worldLoc='-8.0 -20.0 12.0'
fsleyes render --outfile "$output_file" \
        --scene ortho \
        --neuroOrientation \
        --worldLoc $worldLoc \
        --displaySpace "$normalized_structural" \
        --xcentre  0.00000  0.00000 --ycentre  0.00000  0.00000 --zcentre  0.00000  0.00000 \
        --xzoom 100.0 --yzoom 100.0 --zzoom 100.0 \
        --layout $layout \
        --hideCursor \
        --bgColour 0.0 0.0 0.0 --fgColour 1.0 1.0 1.0 \
        --cursorColour 0.0 1.0 0.0 \
        --colourBarLocation top --colourBarLabelSide top-left --colourBarSize 100.0 --labelSize 12 \
        --performance 3 --movieSync "$normalized_structural" --name "subject_structural" \
        --overlayType volume --alpha 100.0 --brightness 50.0 --contrast 50.0 \
        --cmap greyscale --negativeCmap greyscale \
        --interpolation none --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 \
        --clipMode intersection --volume 0 \
        "$resource_dir"/outline_mask.nii --name "outline_mask" --overlayType volume \
        --alpha $alpha --brightness 50.0 --contrast 50.0 \
        --cmap $colormap_alt --negativeCmap greyscale --displayRange 0.0 1.0 --clippingRange 0.0 1.01 --gamma 0.0 \
        --cmapResolution 256 --interpolation none --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 \
        --clipMode intersection --volume 0 \
        "$resource_dir"/m08_m20_p12.mask01.nii.gz --name "m08_m20_p12.mask01" --overlayType volume \
        --alpha $alpha --brightness 50.0 --contrast 50.0 \
        --cmap $colormap --negativeCmap greyscale --displayRange 0.0 1.0 --clippingRange 0.0 1.01 --gamma 0.0 --cmapResolution 256 \
        --interpolation none --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 \
        --clipMode intersection --volume 0 \
        "$resource_dir"/m08_m20_p12.mask02.nii.gz --name "m08_m20_p12.mask02" --overlayType volume \
        --alpha $alpha --brightness 50.0 --contrast 50.0 \
        --cmap $colormap --negativeCmap greyscale --displayRange 0.0 1.0 --clippingRange 0.0 1.01 --gamma 0.0 --cmapResolution 256 \
        --interpolation none --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 \
        --clipMode intersection --volume 0 \
        "$resource_dir"/m08_m20_p12.mask03.nii.gz --name "m08_m20_p12.mask03" --overlayType volume \
        --alpha $alpha --brightness 50.0 --contrast 50.0 \
        --cmap $colormap --negativeCmap greyscale --displayRange 0.0 1.0 --clippingRange 0.0 1.01 --gamma 0.0 --cmapResolution 256 \
        --interpolation none --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 \
        --clipMode intersection --volume 0 \
        "$resource_dir"/m08_m20_p12.mask04.nii.gz --name "m08_m20_p12.mask04" --overlayType volume \
        --alpha $alpha --brightness 50.0 --contrast 50.0 \
        --cmap $colormap --negativeCmap greyscale --displayRange 0.0 1.0 --clippingRange 0.0 1.01 --gamma 0.0 --cmapResolution 256 \
        --interpolation none --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 \
        --clipMode intersection --volume 0 \
        "$resource_dir"/m08_m20_p12.mask05.nii.gz --name "m08_m20_p12.mask05" --overlayType volume \
        --alpha $alpha --brightness 50.0 --contrast 50.0 \
        --cmap $colormap --negativeCmap greyscale --displayRange 0.0 1.0 --clippingRange 0.0 1.01 --gamma 0.0 --cmapResolution 256 \
        --interpolation none --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 \
        --clipMode intersection --volume 0 \
        "$resource_dir"/m08_m20_p12.mask06.nii.gz --name "m08_m20_p12.mask06" --overlayType volume \
        --alpha $alpha --brightness 50.0 --contrast 50.0 \
        --cmap $colormap --negativeCmap greyscale --displayRange 0.0 1.0 --clippingRange 0.0 1.01 --gamma 0.0 --cmapResolution 256 \
        --interpolation none --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 \
        --clipMode intersection --volume 0 

# In short form:
# fsleyes render -of output/render_m8_m20_p12.png \
#         --neuroOrientation \
#         --worldLoc -8 -20 12  \
#         --hideCursor \
#         --performance 3 \
#         "$normalized_structural" \
#         outline_mask.nii -cm $colormap -a $alpha \
#         m08_020_p12.mask01.nii.gz -cm $colormap -a $alpha \
#         m08_p20_p12.mask02.nii.gz -cm $colormap -a $alpha \
#         m08_p20_p12.mask03.nii.gz -cm $colormap -a $alpha \
#         m08_p20_p12.mask04.nii.gz -cm $colormap -a $alpha \
#         m08_p20_p12.mask05.nii.gz -cm $colormap -a $alpha \
#         m08_p20_p12.mask06.nii.gz -cm $colormap -a $alpha

# MNI -30 22 04
# ######################################################################################
output_file="$outputdir"/norm_render_m30_p22_p04.png
worldLoc='-30.0 22.0 4.0'
fsleyes render --outfile "$output_file" \
        --scene ortho \
        --neuroOrientation \
        --worldLoc $worldLoc \
        --displaySpace "$normalized_structural" \
        --xcentre  0.00000  0.00000 --ycentre  0.00000  0.00000 --zcentre  0.00000  0.00000 \
        --xzoom 100.0 --yzoom 100.0 --zzoom 100.0 \
        --layout $layout \
        --hideCursor \
        --bgColour 0.0 0.0 0.0 --fgColour 1.0 1.0 1.0 \
        --cursorColour 0.0 1.0 0.0 \
        --colourBarLocation top --colourBarLabelSide top-left --colourBarSize 100.0 --labelSize 12 \
        --performance 3 --movieSync "$normalized_structural" --name "subject_structural" \
        --overlayType volume --alpha 100.0 --brightness 50.0 --contrast 50.0 \
        --cmap greyscale --negativeCmap greyscale \
        --interpolation none --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 \
        --clipMode intersection --volume 0 \
        "$resource_dir"/outline_mask.nii --name "outline_mask" --overlayType volume \
        --alpha $alpha --brightness 50.0 --contrast 50.0 \
        --cmap $colormap_alt --negativeCmap greyscale --displayRange 0.0 1.0 --clippingRange 0.0 1.01 --gamma 0.0 --cmapResolution 256 \
        --interpolation none --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 \
        --clipMode intersection --volume 0 \
        "$resource_dir"/m30_p22_p04.mask01.nii.gz --name "m30_p22_p04.mask01" --overlayType volume \
        --alpha $alpha --brightness 50.0 --contrast 50.0 \
        --cmap $colormap --negativeCmap greyscale --displayRange 0.0 1.0 --clippingRange 0.0 1.01 --gamma 0.0 --cmapResolution 256 \
        --interpolation none --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 \
        --clipMode intersection --volume 0 \
        "$resource_dir"/m30_p22_p04.mask02.nii.gz --name "m30_p22_p04.mask02" --overlayType volume \
        --alpha $alpha --brightness 50.0 --contrast 50.0 \
        --cmap $colormap --negativeCmap greyscale --displayRange 0.0 1.0 --clippingRange 0.0 1.01 --gamma 0.0 --cmapResolution 256 \
        --interpolation none --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 \
        --clipMode intersection --volume 0 \
        "$resource_dir"/m30_p22_p04.mask03.nii.gz --name "m30_p22_p04.mask03" --overlayType volume \
        --alpha $alpha --brightness 50.0 --contrast 50.0 \
        --cmap $colormap --negativeCmap greyscale --displayRange 0.0 1.0 --clippingRange 0.0 1.01 --gamma 0.0 --cmapResolution 256 \
        --interpolation none --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 \
        --clipMode intersection --volume 0

# In short form
# fsleyes render -of output/render_m30_p22_p04.png \
#         --neuroOrientation \
#         --layout horizontal \
#         --worldLoc -30 22 4  \
#         --hideCursor \
#         --performance 3 \
#         "$normalized_structural" \
#         outline_mask.nii -cm $colormap -a $alpha \
#         m30_p22_p04.mask01.nii.gz -cm $colormap -a $alpha \
#         m30_p22_p04.mask02.nii.gz -cm $colormap -a $alpha \
#         m30_p22_p04.mask03.nii.gz -cm $colormap -a $alpha


# MNI -50 -65 -6
# ######################################################################################
output_file="$outputdir"/norm_render_m50_m65_m06.png
worldLoc='-50.0 -65.0 -6.0'
fsleyes render --outfile "$output_file" \
        --scene ortho \
        --neuroOrientation \
        --worldLoc $worldLoc \
        --displaySpace "$normalized_structural" \
        --xcentre  0.00000  0.00000 --ycentre  0.00000  0.00000 --zcentre  0.00000  0.00000 \
        --xzoom 100.0 --yzoom 100.0 --zzoom 100.0 \
        --layout $layout \
        --hideCursor \
        --bgColour 0.0 0.0 0.0 --fgColour 1.0 1.0 1.0 \
        --cursorColour 0.0 1.0 0.0 \
        --colourBarLocation top --colourBarLabelSide top-left --colourBarSize 100.0 --labelSize 12 \
        --performance 3 --movieSync "$normalized_structural" --name "subject_structural" \
        --overlayType volume --alpha 100.0 --brightness 50.0 --contrast 50.0 \
        --cmap greyscale --negativeCmap greyscale \
        --interpolation none --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 \
        --clipMode intersection --volume 0 \
        "$resource_dir"/outline_mask.nii --name "outline_mask" --overlayType volume \
        --alpha $alpha --brightness 50.0 --contrast 50.0 \
        --cmap $colormap_alt --negativeCmap greyscale --displayRange 0.0 1.0 --clippingRange 0.0 1.01 --gamma 0.0 --cmapResolution 256 \
        --interpolation none --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 \
        --clipMode intersection --volume 0 \
        "$resource_dir"/m50_m65_m06.mask01.nii.gz --name "m50_m65_m06.mask01" --overlayType volume \
        --alpha $alpha --brightness 50.0 --contrast 50.0 \
        --cmap $colormap --negativeCmap greyscale --displayRange 0.0 1.0 --clippingRange 0.0 1.01 --gamma 0.0 --cmapResolution 256 \
        --interpolation none --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 \
        --clipMode intersection --volume 0 \
        "$resource_dir"/m50_m65_m06.mask02.nii.gz --name "m50_m65_m06.mask02" --overlayType volume \
        --alpha $alpha --brightness 50.0 --contrast 50.0 \
        --cmap $colormap --negativeCmap greyscale --displayRange 0.0 1.0 --clippingRange 0.0 1.01 --gamma 0.0 --cmapResolution 256 \
        --interpolation none --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 \
        --clipMode intersection --volume 0 \
        "$resource_dir"/m50_m65_m06.mask03.nii.gz --name "m50_m65_m06.mask03" --overlayType volume \
        --alpha $alpha --brightness 50.0 --contrast 50.0 \
        --cmap $colormap --negativeCmap greyscale --displayRange 0.0 1.0 --clippingRange 0.0 1.01 --gamma 0.0 --cmapResolution 256 \
        --interpolation none --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 \
        --clipMode intersection --volume 0

# In short form
# fsleyes render -of output/render_m50_m65_m06.png \
#        --neuroOrientation \
#        --layout horizontal \
#        --worldLoc -50 -65 -6  \
#        --hideCursor \
#        --performance 3 \
#        "$normalized_structural" \
#        outline_mask.nii -cm $colormap -a $alpha \
#        m50_m65_m06.mask01.nii.gz -cm $colormap -a $alpha \
#        m50_m65_m06.mask02.nii.gz -cm $colormap -a $alpha \
#        m50_m65_m06.mask03.nii.gz -cm $colormap -a $alpha

# MNI 30 54 58
# ########################################################################################
output_file="$outputdir"/norm_render_p30_p54_p58.png
worldLoc='30.0 54.0 58.0'
fsleyes render --outfile "$output_file" \
        --scene ortho \
        --neuroOrientation \
        --worldLoc $worldLoc \
        --displaySpace "$normalized_structural" \
        --xcentre  0.00000  0.00000 --ycentre  0.00000  0.00000 --zcentre  0.00000  0.00000 \
        --xzoom 100.0 --yzoom 100.0 --zzoom 100.0 \
        --layout $layout \
        --hideCursor \
        --bgColour 0.0 0.0 0.0 --fgColour 1.0 1.0 1.0 --cursorColour 0.0 1.0 0.0 \
        --colourBarLocation top --colourBarLabelSide top-left --colourBarSize 100.0 \
        --labelSize 12 --performance 3 --movieSync "$normalized_structural" --name "subject_structural" \
        --overlayType volume --alpha 100.0 --brightness 50.0 --contrast 50.0 \
        --cmap greyscale --negativeCmap greyscale \
        --interpolation none --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 \
        --clipMode intersection --volume 0 \
        "$resource_dir"/outline_mask.nii --name "outline_mask" --overlayType volume \
        --alpha $alpha --brightness 50.0 --contrast 50.0 \
        --cmap $colormap_alt --negativeCmap greyscale --displayRange 0.0 1.0 --clippingRange 0.0 1.01 --gamma 0.0 --cmapResolution 256 \
        --interpolation none --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 \
        --clipMode intersection --volume 0 \
        "$resource_dir"/p30_p54_p58.mask01.nii.gz --name "p30_p54_p58.mask01" --overlayType volume \
        --alpha $alpha --brightness 50.0 --contrast 50.0 \
        --cmap $colormap --negativeCmap greyscale --displayRange 0.0 1.0 --clippingRange 0.0 1.01 --gamma 0.0 --cmapResolution 256 \
        --interpolation none --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 \
        --clipMode intersection --volume 0

# In short form
# fsleyes render -of output/render_p30_p54_p58.png \
#         --neuroOrientation \
#         --layout horizontal \
#         --worldLoc 30 54 58 \
#         --hideCursor \
#         --performance 3 \
#         "$normalized_structural" \
#         outline_mask.nii -cm $colormap -a $alpha \
#         p30_p54_p58.mask01.nii.gz -cm $colormap -a $alpha

# Now, overlay a mean functional over template

outfile="$outputdir"/norm_functional_ortho_overlay.png
worldLoc='0.0 0.0 24.0'
fsleyes render --outfile "$outfile" --scene ortho --neuroOrientation --worldLoc $worldLoc \
        --displaySpace "$resource_dir"/avg152T1.nii --xcentre  0.00000  0.00000 --ycentre  0.00000  0.00000 --zcentre  0.00000  0.00000 \
        --xzoom 100.0 --yzoom 100.0 --zzoom 100.0 --layout horizontal --hideCursor \
        --bgColour 0.0 0.0 0.0 --fgColour 1.0 1.0 1.0 --cursorColour 0.0 1.0 0.0 --colourBarLocation top --colourBarLabelSide top-left \
        --colourBarSize 100.0 --labelSize 12 --performance 3 --movieSync "$resource_dir"/avg152T1.nii --name "avg152T1" \
        --overlayType volume --alpha 100.0 --brightness 50.0 --contrast 50.0 --cmap greyscale --negativeCmap greyscale \
        --displayRange 0.0 1.0 --clippingRange 0.0 1.01 --gamma 0.0 --cmapResolution 256 --interpolation none \
        --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 --clipMode intersection \
        --volume 0 \
        "$mean_functional" --name "mean_normalized_functional" --overlayType volume --alpha 33.0 --brightness 50.0 --contrast 50.0 \
        --cmap brain_colours_1hot_iso --negativeCmap greyscale --gamma 0.0 --cmapResolution 256 --interpolation none --numSteps 100 \
        --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 --clipMode intersection --volume 0

outfile="$outputdir"/norm_functional_lightbox_overlay.png
worldLoc='0.0 0.0 24.0'
fsleyes render --outfile "$outfile" --scene lightbox --neuroOrientation --worldLoc $worldLoc \
        --displaySpace  "$resource_dir"/avg152T1.nii --zaxis 1 --sliceSpacing 5 --zrange -1.0 217 --ncols 8 --nrows 5 \
        --bgColour 0.0 0.0 0.0 --fgColour 1.0 1.0 1.0 --cursorColour 0.0 1.0 0.0 --colourBarLocation top --colourBarLabelSide top-left \
        --colourBarSize 100.0 --labelSize 12 --performance 3 --movieSync "$resource_dir"/avg152T1.nii --name "avg152T1" \
        --overlayType volume --alpha 100.0 --brightness 50.0 --contrast 50.0 --cmap greyscale --negativeCmap greyscale \
        --displayRange 0.0 1.0 --clippingRange 0.0 1.01 --gamma 0.0 --cmapResolution 256 --interpolation none \
        --numSteps 100 --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 --clipMode intersection --volume 0 \
        "$mean_functional" --name "mean_normalized_functional" --overlayType volume --alpha 33.0 --brightness 50.0 --contrast 50.0 \
        --cmap brain_colours_1hot_iso --negativeCmap greyscale --gamma 0.0 --cmapResolution 256 --interpolation none --numSteps 100 \
        --blendFactor 0.1 --smoothing 0 --resolution 100 --numInnerSteps 10 --clipMode intersection --volume 0
