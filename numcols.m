function n = numcols(x)
% N = NUMCOLS  Returns the number of columns of a 2d data object
%     X <2d data object>: for example, a matrix, cell array, dataset or
%     table.
%
n = size(x, 2);
end
